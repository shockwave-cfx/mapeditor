// eslint-disable-next-line import/prefer-default-export
export const moveToTop = (windows, id) => {
  const window = windows.find(x => x.id === id);
  const layers = windows.filter(x => x.id !== id);
  return [...layers, window];
};
