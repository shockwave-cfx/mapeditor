import React from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';

import Portal from './Portal';
import { moveToTop } from './utils';

//-----------------------------------------------------------------------------

// TODO: For testing purposes.
/* eslint-disable no-console */
const defaultValue = {
  windowDidMount: (...args) => console.log('windowDidMount', args),
  windowDidUpdate: (...args) => console.log('windowDidUpdate', args),
  windowWillUnmount: (...args) => console.log('windowWillUnmount', args)
};
/* eslint-enable no-console */

// Exported to be used in the `Window` component.
// Context is used to manage the window stack.
export const WindowContext = React.createContext(defaultValue);
export const WindowConsumer = WindowContext.Consumer;

// ----------------------------------------------------------------------------

class WindowProvider extends React.Component {
  constructor() {
    super();

    this.state = {
      windows: []
    };

    this.providerValue = {
      windowDidMount: this.windowDidMount.bind(this),
      windowDidUpdate: this.windowDidUpdate.bind(this),
      windowWillUnmount: this.windowWillUnmount.bind(this)
    };

    this.onFocus = this.onFocus.bind(this);
    this.renderPortal = this.renderPortal.bind(this);
  }

  onFocus(id) {
    this.setState((state) => {
      const windows = moveToTop(state.windows, id);
      return { windows };
    });
  }

  windowDidMount(id, props) {
    this.setState((state) => {
      const window = { id, props };
      const windows = [...state.windows, window];
      return { windows };
    });
  }

  windowDidUpdate(id, props, prevProps) {
    const updateProps = (state) => {
      const update = x => (x.id !== id ? x : { ...x, props });
      const windows = state.windows.map(update);
      return { windows };
    };

    const updateLayers = (state) => {
      if (props.open && !prevProps.open) {
        const windows = moveToTop(state.windows, id);
        return { windows };
      }

      if (!props.open && prevProps.open && state.windows[0].id === id) {
        const windows = moveToTop(state.windows, state.windows[1].id);
        return { windows };
      }

      return state;
    };

    this.setState((state) => {
      const update = compose(updateProps, updateLayers);
      return update(state);
    });
  }

  windowWillUnmount(id) {
    this.setState((state) => {
      const filter = x => x.id === id;
      const windows = state.windows.filter(filter);
      return { windows };
    });
  }

  renderPortal(window, zIndex, windows) {
    const { id, props } = window;
    const active = zIndex === windows.length - 1;

    return (
      <Portal
        key={id}
        id={id}
        active={active}
        zIndex={zIndex}
        onFocus={this.onFocus}
        {...props}
      />
    );
  }

  render() {
    return (
      <WindowContext.Provider value={this.providerValue}>
        {this.props.children}
        {this.state.windows.map(this.renderPortal)}
      </WindowContext.Provider>
    );
  }
}

//-----------------------------------------------------------------------------

WindowProvider.propTypes = {
  children: PropTypes.node
};

WindowProvider.defaultProps = {
  children: null
};

//-----------------------------------------------------------------------------

export default WindowProvider;
