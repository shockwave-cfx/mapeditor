import React from 'react';
import shortid from 'shortid';

import { WindowContext } from './Provider';

/**
 * The Window component does not actually render anything. Rendering is
 * handled by the Context component, which will create all Portals for us.
 *
 * That way we can have a single Window manager without tangling props all
 * around. The downside is that we pull off a bit of magic, but it works.
 */
class Window extends React.Component {
  constructor(props) {
    super(props);

    // Each Window should have a unique identifier. Symbols don't work in
    // React keys, so we use a random shortid.
    this.id = shortid.generate();
  }

  componentDidMount() {
    this.context.windowDidMount(this.id, this.props);
  }

  componentDidUpdate(prevProps) {
    this.context.windowDidUpdate(this.id, this.props, prevProps);
  }

  componentWillUnmount() {
    this.context.windowWillUnmount(this.id);
  }

  // TODO: I would actually prefer to render the portal here. All the magic is
  // making my head dizzy.
  // Rendering is now handled by the Provider component.
  render() {
    return null;
  }
}

Window.contextType = WindowContext;

export default Window;
