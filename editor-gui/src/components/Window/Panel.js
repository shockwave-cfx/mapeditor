import styled, { css } from 'styled-components';

const INITIAL_LAYER = 1000;
const display = props => (props.isVisible ? 'block' : 'none');
const zIndex = props => props.zIndex + INITIAL_LAYER;

export default styled.div`
  position: absolute;
  padding: 4px;
  color: white;
  border-radius: 4px;
  background-color: #31353b;
  box-shadow: 0 0 25px 0 rgba(0, 0, 0, 0.75);
  display: ${display};
  z-index: ${zIndex};

  transition:
    opacity 0.065s ease-out,
    background-color 0.065s ease-out,
    box-shadow 0.065s ease-out,
    filter 0.065s ease-out;

  ${props => !props.isActive && css`
    opacity: 0.85;
    background-color: #2d2d2d;
    box-shadow: 0 0 5px 0 rgba(0, 0, 0, 0.45);
    filter: grayscale(50%);
  `}
`;
