import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { lighten } from 'polished';

import Icon from '../Icon';

// ----------------------------------------------------------------------------

const Component = (props) => {
  const {
    icon,
    children,

    // unused
    round,
    active,

    // rest
    ...rest
  } = props;

  return (
    <button type="button" {...rest}>
      {icon && <Icon name={icon} />}
      {!icon && children}
    </button>
  );
};

Component.propTypes = {
  active: PropTypes.bool,
  round: PropTypes.bool,
  icon: PropTypes.string,
  children: PropTypes.node
};

Component.defaultProps = {
  active: false,
  round: false,
  icon: null,
  children: null
};

// ----------------------------------------------------------------------------

export default styled(Component)`
  cursor: pointer;
  padding: 0 12px;
  outline: none;
  border: none;
  text-align: center;
  box-shadow: 0px 0px 5px 0px rgba(0, 0, 0, 0.25);
  border-radius: 4px;
  font-size: 100%;

  height: 36px;
  line-height: 36px;

  ${props => !props.active && `
    color: white;
    background-color: ${props.theme.colors.primary};
  `}

  ${props => props.active && `
    color: ${props.theme.colors.primary};
    background-color: white;
  `}

  ${props => props.icon && props.round && 'border-radius: 50%;'}
  ${props => props.icon && `
    padding: 0;
    width: 36px;
  `}

  :hover {
    color: white;
    background-color: ${props => lighten(0.1, props.theme.colors.primary)};
  }
`;
