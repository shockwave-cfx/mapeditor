import { SET_GUI_VISIBLE, SET_GUI_ACTIVE } from './actions';
import { IS_BROWSER } from '../../constants';

const defaultState = {
  isVisible: IS_BROWSER,
  isActive: IS_BROWSER
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case SET_GUI_VISIBLE: {
      return {
        ...state,
        isVisible: action.payload.visible
      };
    }

    // ------------------------------------------------------------------------

    case SET_GUI_ACTIVE: {
      return {
        ...state,
        isActive: action.payload.active
      };
    }

    // ------------------------------------------------------------------------

    default:
      return state;
  }
};
