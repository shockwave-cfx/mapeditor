import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { setGuiVisible, setGuiActive } from '../actions';
import Nui from '../../../game/Nui';

const areNuiControlsLocked = () => (
  document.activeElement &&
  document.activeElement.dataset.lockNuiControls
);

export class NuiHandlerComponent extends React.Component {
  constructor() {
    super();

    // This only needs to be called once, not every mount. Calling NUI at this
    // stage tells the game our UI is ready to interact with.
    Nui.ready();

    this.onMessage = this.onMessage.bind(this);
    this.onKeydown = this.onKeydown.bind(this);
  }

  componentDidMount() {
    window.addEventListener('message', this.onMessage);
    window.addEventListener('keydown', this.onKeydown);
  }

  componentWillUnmount() {
    window.removeEventListener('message', this.onMessage);
    window.removeEventListener('keydown', this.onKeydown);
  }

  onMessage(event) {
    if (event.data.command === 'setVisible') {
      const isVisible = event.data.args[0];
      this.props.setGuiVisible(isVisible);
    }

    if (event.data.command === 'setActive') {
      const isActive = event.data.args[0];
      this.props.setGuiActive(isActive);
    }
  }

  // eslint-disable-next-line class-methods-use-this
  onKeydown(event) {
    if (areNuiControlsLocked()) {
      return;
    }

    switch (event.key.toLowerCase()) {
      case 'f':
      case 'escape':
        Nui.blur();
        break;
      case 'f5':
        Nui.quit();
        break;
      default:
        break;
    }
  }

  render() {
    return null;
  }
}

NuiHandlerComponent.propTypes = {
  setGuiVisible: PropTypes.func.isRequired,
  setGuiActive: PropTypes.func.isRequired
};

export default connect(null, {
  setGuiVisible,
  setGuiActive
})(NuiHandlerComponent);
