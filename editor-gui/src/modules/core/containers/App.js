import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { isGuiVisible } from '../selectors';

import GlobalStyle from '../components/GlobalStyle';
import Root from '../components/Root';
import DevTools from './DevTools';
import NuiHandler from './NuiHandler';

import Toolbar from '../../toolbar/containers/Toolbar';
import Browser from '../../browser/containers/Browser';

export const AppComponent = props => (
  <Root {...props}>
    <GlobalStyle />
    <DevTools />
    <NuiHandler />
    <Toolbar />
    <Browser />
  </Root>
);

AppComponent.propTypes = {
  isVisible: PropTypes.bool.isRequired
};

export default connect(state => ({
  isVisible: isGuiVisible(state)
}))(AppComponent);
