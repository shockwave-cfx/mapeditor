import { createGlobalStyle, css } from 'styled-components';
import { IS_BROWSER } from '../../../constants';

export default createGlobalStyle`
  html,
  body,
  #root {
    margin: 0;
    padding: 0;
    width: 100%;
    height: 100%;
    overflow: hidden;
  }

  * {
    font-family: Roboto, sans-serif;
    box-sizing: border-box;
    user-select: none;
  }

  ${IS_BROWSER && css`
    body {
      background-image: url('/public/images/ingame.jpg');
      background-size: cover;
      background-position: center center;
    }
  `}
`;
