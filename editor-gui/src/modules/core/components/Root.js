import styled from 'styled-components';
import PropTypes from 'prop-types';

const Root = styled.div`
  opacity: ${props => +props.isVisible};
  width: 100%;
  height: 100%;
  overflow: hidden;
  transition: opacity 100ms ease-out;
`;

Root.propTypes = {
  isVisible: PropTypes.bool.isRequired
};

export default Root;
