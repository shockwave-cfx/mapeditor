import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import Nui from '../../../game/Nui';
import Button from '../../../components/Button';

// ----------------------------------------------------------------------------

const onClick = props => (...args) => {
  const soundBank = 'HUD_FRONTEND_DEFAULT_SOUNDSET';
  const soundName = 'SELECT';

  Nui.playSound(soundBank, soundName);
  return props.onClick && props.onClick(...args);
};

const onMouseEnter = props => (...args) => {
  const soundBank = 'HUD_FRONTEND_DEFAULT_SOUNDSET';
  const soundName = 'NAV_UP_DOWN';

  Nui.playSound(soundBank, soundName);
  return props.onMouseEnter && props.onMouseEnter(...args);
};

// ----------------------------------------------------------------------------

export const Wrapper = styled.div`
  display: block;
  font-size: 22px;
  border-radius: 4px;

  &:not(:last-child) {
    margin-bottom: 16px;
  }

  ${props => props.active && `
    padding: 4px;
    background-color: rgba(0, 0, 0, 0.45);
    box-shadow: 0px 0px 5px 0px rgba(0, 0, 0, 0.25);
  `}

`;

// ----------------------------------------------------------------------------

export const ButtonComponent = styled(Button)`
  width: 48px;
  height: 48px;
  line-height: 48px;

  ${props => props.active && `
    border-radius: 2px;

    width: 40px;
    height: 40px;
    line-height: 40px;
  `}
`;

// ----------------------------------------------------------------------------

const Component = props => (
  <Wrapper active={props.active}>
    <ButtonComponent
      {...props}
      onClick={onClick(props)}
      onMouseEnter={onMouseEnter(props)}
    />
  </Wrapper>
);

Component.propTypes = {
  active: PropTypes.bool.isRequired,
  onClick: PropTypes.func,
  onMouseEnter: PropTypes.func
};

Component.defaultProps = {
  onClick: null,
  onMouseEnter: null
};

export default Component;
