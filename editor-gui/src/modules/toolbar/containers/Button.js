import { connect } from 'react-redux';

import Button from '../components/Button';

// TODO: Seems rather complicated to achieve something simple.
// This connect simply maps the button props to the Button component.
export default connect((state, ownProps) => ({
  active: ownProps.isActive(state)
}), (dispatch, ownProps) => ({
  setActive: active => dispatch(ownProps.setActive(active))
}), (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  onClick: () => dispatchProps.setActive(!stateProps.active),
  icon: !stateProps.active
    ? ownProps.icon
    : 'close'
}))(Button);
