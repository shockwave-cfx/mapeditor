import { SET_TOOLBAR_VISIBLE } from './actions';

const defaultState = {
  isVisible: true
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case SET_TOOLBAR_VISIBLE: {
      return {
        ...state,
        isVisible: action.payload.visible
      };
    }

    // ------------------------------------------------------------------------

    default:
      return state;
  }
};
