export const SET_TOOLBAR_VISIBLE = 'TOOLBAR/SET_TOOLBAR_VISIBLE';

export function setToolbarVisible(visible) {
  const type = SET_TOOLBAR_VISIBLE;
  const payload = { visible };
  return { type, payload };
}
