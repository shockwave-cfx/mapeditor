import { SET_MENU_VISIBLE } from './actions';

const defaultState = {
  isVisible: false
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case SET_MENU_VISIBLE: {
      return {
        ...state,
        isVisible: action.payload.visible
      };
    }

    // ------------------------------------------------------------------------

    default:
      return state;
  }
};
