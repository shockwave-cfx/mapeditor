import { createSelector } from 'reselect';
import { isGuiVisible as getIsGuiVisible } from '../core/selectors';

export const getMenuState = state => state.menu;
export const isMenuVisible = createSelector(
  getMenuState,
  getIsGuiVisible,
  (menu, isGuiVisible) => (
    isGuiVisible &&
    menu.isVisible
  )
);
