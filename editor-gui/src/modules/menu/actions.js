export const SET_MENU_VISIBLE = 'MENU/SET_MENU_VISIBLE';

export function setMenuVisible(visible) {
  const type = SET_MENU_VISIBLE;
  const payload = { visible };
  return { type, payload };
}
