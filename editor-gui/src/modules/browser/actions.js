export const SET_BROWSER_VISIBLE = 'BROWSER/SET_BROWSER_VISIBLE';

export function setBrowserVisible(visible) {
  const type = SET_BROWSER_VISIBLE;
  const payload = { visible };
  return { type, payload };
}
