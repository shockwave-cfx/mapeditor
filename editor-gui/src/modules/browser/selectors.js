import { createSelector } from 'reselect';
import { isGuiVisible as getIsGuiVisible } from '../core/selectors';

export const getBrowserState = state => state.browser;
export const isBrowserVisible = createSelector(
  getBrowserState,
  getIsGuiVisible,
  (browser, isGuiVisible) => (
    isGuiVisible &&
    browser.isVisible
  )
);
