import axios from 'axios';
import { IS_BROWSER, RESOURCE_NAME } from '../constants';

const Nui = axios.create({
  baseURL: `http://${RESOURCE_NAME}/`
});

// ----------------------------------------------------------------------------

Nui.call = (path, ...args) => {
  // In browser environments, just mock the request.
  if (IS_BROWSER) {
    // eslint-disable-next-line no-console
    console.log(`NUI call(${path})`, args);
    return Promise.resolve();
  }

  // In NUI, we use posts.
  return Nui.post(path, ...args);
};

// ----------------------------------------------------------------------------

/* eslint-disable implicit-arrow-linebreak */
Nui.ready = () =>
  Nui.call('ready');

Nui.blur = () =>
  Nui.call('blur');

Nui.quit = () =>
  Nui.call('quit');

Nui.playSound = (soundBank, soundName) =>
  Nui.call('playSound', { soundBank, soundName });
/* eslint-enable implicit-arrow-linebreak */

// ----------------------------------------------------------------------------

export default Nui;
