import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';

import Root from '../../../../src/modules/core/components/Root';

// TODO: Fix Enzyme + styled-components testing.
describe.skip('<Root />', () => {
  it('should render correctly', () => {
    const node = shallow(<Root isVisible />);
    expect(node).to.match.snapshot();
  });

  //----------------------------------------------------------------------------

  it('should change opacity for isVisible', () => {
    [true, false].forEach((isVisible) => {
      const node = shallow(<Root isVisible={isVisible} />);
      expect(node).to.have.style('opacity', +isVisible);
    });
  });
});
