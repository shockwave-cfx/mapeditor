import React from 'react';
import { mount } from 'enzyme';
import { expect } from 'chai';

// Since we are changing constants, we need to reload the component file for
// changes to have effect. Therefor, `require` is used over `import` here.
const componentPath = '../../../../src/modules/core/components/GlobalStyle';
const constantsPath = '../../../../src/constants';

// Mutable variable as substitution for imports.
let GlobalStyle;

// TODO: Fix Enzyme + styled-components testing.
describe.skip('<GlobalStyle />', () => {
  beforeEach(() => {
    // Delete global pollution before each test.
    delete global.window.GetParentResourceName;

    // Clear cache to force reloads between requires.
    // https://nodejs.org/docs/v11.10.0/api/modules.html#modules_require_cache
    // TODO: Maybe consider https://github.com/dwyl/decache
    delete require.cache[require.resolve(componentPath)];
    delete require.cache[require.resolve(constantsPath)];

    // Re-require GlobalStyle component.
    // eslint-disable-next-line global-require, import/no-dynamic-require
    GlobalStyle = require(componentPath).default;
  });

  //----------------------------------------------------------------------------

  it.skip('should render correctly', () => {
    const node = mount(<GlobalStyle />);
    expect(node).to.match.snapshot();
  });

  //----------------------------------------------------------------------------

  it('should render not render a background in nui', () => {
    const node = mount(<GlobalStyle />);
    expect(node).to.not.have.style('background-image');
  });

  //----------------------------------------------------------------------------

  it('should render a background in the browser', () => {
    // Force the `IS_BROWSER` constant to `true`.
    global.window.GetParentResourceName = () => '';

    const node = mount(<GlobalStyle />);
    expect(node).to.have.style('background-image');
  });
});
