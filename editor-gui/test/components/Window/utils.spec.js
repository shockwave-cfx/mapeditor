import { expect } from 'chai';
import { moveToTop } from '../../../src/components/Window/utils';

describe('utils.moveToTop', () => {
  it('should move an item in an array to the top of the stack', () => {
    const input = [
      { id: 'A' },
      { id: 'B' },
      { id: 'C' },
      { id: 'D' }
    ];

    const output = [
      { id: 'A' },
      { id: 'C' },
      { id: 'D' },
      { id: 'B' }
    ];

    const moveId = 'B';
    const result = moveToTop(input, moveId);

    expect(result).to.deep.equal(output);
  });
});
