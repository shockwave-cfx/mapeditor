resource_manifest_version '44febabe-d386-4d18-afbe-5e627f4af937'

client_script 'client/utils.lua'
client_script 'client/config.lua'
client_script 'client/camera.lua'
client_script 'client/exports.lua'
client_script 'client/main.lua'
